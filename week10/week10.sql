describe Customers;

select *
from Customers
order by CustomerName desc;


create index abc on Customers(CustomerName);


explain select *
from Customers
order by CustomerName desc;

alter table Customers drop index abc;

create view myview as select *
from Customers;

select * 
from myview;

insert into myview values(8,"s","l","h","a",0,"y");

select * from Customers;